import React from 'react'
import { Formik, Form } from 'formik';
import {
    Box,
    Button
} from "@chakra-ui/react"
import { Wrapper } from '../components/Wrapper';
import { InputField } from '../components/InputField';
import { useLoginMutation } from '../generated/graphql';
import { toErrorMap } from '../utils/toErrorMap';
import { useRouter } from 'next/dist/client/router';


interface loginProps {

}

export const Login: React.FC<loginProps> = () => {
    const [, login] = useLoginMutation();
    const router = useRouter();
    return (
        <Wrapper variant="small">
            <Formik
                initialValues={{ username: '', password: '' }}
                onSubmit={async (values, { setErrors }) => {
                    const response = await login(values);
                    if (response.data?.login.errors) {
                        setErrors(toErrorMap(response.data.login.errors));
                    }
                    else if (response.data?.login.user) {
                        router.push("/");
                    }
                }}
            >
                {
                    ({ isSubmitting }) => (
                        <Form>
                            <InputField name="username" label="Username" placeholder="username" />
                            <Box>
                                <InputField name="password" label="Password" placeholder="password" type="password" />
                            </Box>
                            <Button mt={4} colorScheme="teal" isLoading={isSubmitting} type="submit">
                                Login
                            </Button>
                        </Form>
                    )
                }
            </Formik>
        </Wrapper>
    );
}

export default Login;